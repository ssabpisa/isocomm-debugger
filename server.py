#!/usr/bin/env python

# Python socket programming
# Simple Echo Server
import socket
# Create socket by socket(family, type [,protocol])
# ======================== FAMILY ==========================
# AF_INET = IPv4, address represented using tuple (host, port) 
# where host and port are string and int respectively
# AF_INET6 = IPv6
# ======================== TYPE ============================
# SOCK_STREAM = TCP
# SOCK_DGRAM = UDP
# ========================= SERVER ===========================
# Step 1) Create a socket
new_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM);
# Step 2) Bind the socket to an address and port
port_num = 9000; 
host = '192.168.1.10'; # setting to empty string tells bind() to fill in the
		   # the address of the current machine
new_socket.bind((host,port_num));

# Step 3) Listen for incoming connection
backlog = 5; # number of clients allowed to wait while server is
			 # handling the current client
new_socket.listen(backlog);
# Step 4) Wait for clients & # Step 5) Accept a client
while True:
	client,address = new_socket.accept();
	# client = new socket object
	# address = address of client
	max_size = 1024;
	data = client.recv(max_size); # maximum data in bytes socket is
								 # willing to receive
# Step 6) Send and receive data
	print "I see you, <%s>: <%s>\n" % (str(address), str(data));
	cmd = str(data).split("=")[1];
	if(cmd == 'END'):
		client.send("Its over between us. Goodbye.\n");
		client.close();
		break;
	elif(cmd == 'GPS'):
		f = open('simulatelatlon');
		#TODO: make it more efficient
		j = f.read().split("\n");
		print "Got coordinate request %s \n" % str(address);
		client.send(j[len(j)-2]); #send() sends data to client and returns
						   #number of bytes that are sent
	elif(cmd == 'TEST'):
		print "Got Test command %s \n" % str(address);
		client.send("Hello from Server"); 
	else:
		client.send("Request not recognized\n");
	
	client.close(); #close the socket
