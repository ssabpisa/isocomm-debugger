#!/usr/bin/env python

# Python socket programming
# Simple Echo Client
import socket, sys
# Create socket by socket(family, type [,protocol])
# ======================== FAMILY ==========================
# AF_INET = IPv4, address represented using tuple (host, port) 
# where host and port are string and int respectively
# AF_INET6 = IPv6
# ======================== TYPE ============================
# SOCK_STREAM = TCP
# SOCK_DGRAM = UDP
# ========================= CLIENT ===========================
# Step 1) Create socket
new_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM);
# Step 2) Connect to server
#host = socket.gethostname();
port = 9000;
max_size = 1024;
new_socket.connect((socket.gethostname(), port));
# Step 3) Send and Receive Data
new_socket.send(sys.argv[1]);
data = new_socket.recv(max_size);
new_socket.close();

print ("RESPONSE: " + str(data));